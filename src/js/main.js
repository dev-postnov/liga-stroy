/*
 * Third party
 */

 //import('./../../bower_components/jquery/dist/jquery.min.js');


/*
 * Custom
 */


//check contact page
if (document.querySelector('.maps')) {

    var mapsTab = document.querySelectorAll('.maps__tab'),
        mapsContent = document.querySelectorAll('.maps__content');

        mapsTab.forEach(function(tab, i) {
          tab.addEventListener('click', function() {
            var tabId = this.getAttribute('data-tab'),
                toggleContent = document.querySelector('.maps__content[data-tab="'+tabId+'"]'),
                activeTab = document.querySelector('.maps__tab.active'),
                activeContent = document.querySelector('.maps__content.active');

            activeTab.classList.remove('active');
            this.classList.add('active');

            activeContent.classList.remove('active');
            toggleContent.classList.add('active');
          });
        });
}


function initMap() {
  var barnaul = {
      lat: 53.368056,
      lng: 83.760606
  };


  var map = new google.maps.Map(document.getElementById('footer-map'), {
      zoom: 17,
      center: barnaul
  });

  var marker = new google.maps.Marker({
      position: barnaul,
      map: map
  });
}




//categories

$('.categories__title').click(function() {
    $(this).closest('.categories__block').find('.categories__list').slideToggle();
})



//nav

$('.icon-menu').click(function() {
    $('.header__nav').addClass('active');
})

$('.icon-cross').click(function() {
    $('.header__nav').removeClass('active');
})